## RMF-assets

`C:\_\keyprism3\._plato\assets`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `175`
- `thought_uid` - `1BAB47EA-C39C-EFA6-7E39-A188A7E81769`
- `vp_url` - `keyprism3.vpp://shape/CRF4SNqGAqAAAhN0/.zt4SNqGAqAAAhOD`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/ZxSviNqGAqAAAhKL`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets) _Plato styles, fonts and scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%34%26webthought_id%3D%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2Fk.%35%38SNqGAqAAAhXC%2F%38MN%38SNqGAqAAAhXO%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fo%34jOPVqGAqAAAh%30C) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FCRF%34SNqGAqAAAhN%30%2F.zt%34SNqGAqAAAhOD) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FCRF%34SNqGAqAAAhN%30%2F.zt%34SNqGAqAAAhOD) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%31BAB%34%37EA-C%33%39C-EFA%36-%37E%33%39-A%31%38%38A%37E%38%31%37%36%39) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-175) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_CRF4SNqGAqAAAhN0.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%36%26webthought_id%3D%31%37%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%33QvESNqGAqAAAhPM%2FvG%34kSNqGAqAAAhPb%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FSKZviNqGAqAAAhKa)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%36%26webthought_id%3D%31%37%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%33QvESNqGAqAAAhPM%2FvG%34kSNqGAqAAAhPb%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FSKZviNqGAqAAAhKa) `css` - Plato Styles
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss+-+Copy%5CDesktop.ini)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss+-+Copy%5CDesktop.ini) `css - Copy`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cfont%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%38%26webthought_id%3D%31%37%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FEmZUSNqGAqAAAhRC%2FmAVUSNqGAqAAAhRP%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fb.IfiNqGAqAAAhK%38)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cfont%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%38%26webthought_id%3D%31%37%38%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FEmZUSNqGAqAAAhRC%2FmAVUSNqGAqAAAhRP%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fb.IfiNqGAqAAAhK%38) `font` - Plato Fonts - FontAwesome
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%39%26webthought_id%3D%31%37%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FD%39m%30SNqGAqAAAhRx%2F%35bu%30SNqGAqAAAhR.%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fl%39CfiNqGAqAAAhLP)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%39%26webthought_id%3D%31%37%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FD%39m%30SNqGAqAAAhRx%2F%35bu%30SNqGAqAAAhR.%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fl%39CfiNqGAqAAAhLP) `scripts` - Plato Scripts and Script Directories

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5CREADME.html) `README.html`

