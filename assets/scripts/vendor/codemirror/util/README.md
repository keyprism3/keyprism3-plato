## README

`C:\_\keyprism3\._plato\assets\scripts\vendor\codemirror\util`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `183`
- `thought_uid` - `522C5FF3-EC33-30C9-234E-341A06E2AABA`
- `vp_url` - `keyprism3.vpp://shape/anacSNqGAqAAAhVY/T1mcSNqGAqAAAhVn`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/Krg_iNqGAqAAAhMR`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil) _Plato Codemirror Library Script Utils_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%32%26webthought_id%3D%31%38%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FVqlsSNqGAqAAAhUa%2FrNNsSNqGAqAAAhUp%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fds%37fiNqGAqAAAhMC) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FanacSNqGAqAAAhVY%2FT%31mcSNqGAqAAAhVn) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FanacSNqGAqAAAhVY%2FT%31mcSNqGAqAAAhVn) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%35%32%32C%35FF%33-EC%33%33-%33%30C%39-%32%33%34E-%33%34%31A%30%36E%32AABA) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/scripts/vendor/codemirror/util/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-183) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_anacSNqGAqAAAhVY.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cclosetag.js) `closetag.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccolorize.js) `colorize.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccontinuecomment.js) `continuecomment.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Ccontinuelist.js) `continuelist.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cdialog.css) `dialog.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cdialog.js) `dialog.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cfoldcode.js) `foldcode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cformatting.js) `formatting.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cjavascript-hint.js) `javascript-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cloadmode.js) `loadmode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmatch-highlighter.js) `match-highlighter.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmatchbrackets.js) `matchbrackets.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cmultiplex.js) `multiplex.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Coverlay.js) `overlay.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cpig-hint.js) `pig-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Crunmode-standalone.js) `runmode-standalone.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Crunmode.js) `runmode.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csearch.js) `search.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csearchcursor.js) `searchcursor.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csimple-hint.css) `simple-hint.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Csimple-hint.js) `simple-hint.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5Cxml-hint.js) `xml-hint.js`

