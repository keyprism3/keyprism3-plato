## RMF-assets-scripts-vendor-codemirror

`C:\_\keyprism3\._plato\assets\scripts\vendor\codemirror`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `182`
- `thought_uid` - `15331651-E235-2D9C-5963-052504859670`
- `vp_url` - `keyprism3.vpp://shape/VqlsSNqGAqAAAhUa/rNNsSNqGAqAAAhUp`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/ds7fiNqGAqAAAhMC`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror) _Plato Codemirror Library Scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%31%26webthought_id%3D%31%38%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FZlPMSNqGAqAAAhTq%2FsnAsSNqGAqAAAhT%33%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F.NlfiNqGAqAAAhLv) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FVqlsSNqGAqAAAhUa%2FrNNsSNqGAqAAAhUp) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FVqlsSNqGAqAAAhUa%2FrNNsSNqGAqAAAhUp) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%31%35%33%33%31%36%35%31-E%32%33%35-%32D%39C-%35%39%36%33-%30%35%32%35%30%34%38%35%39%36%37%30) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/scripts/vendor/codemirror/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-182) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/esdoc-16.png)](http://mrobbinsassoc.com/projects/keyprism3/esdoc/file/._plato/assets/scripts/vendor/codemirror/codemirror.js.html) - esdoc
- [![](http://mrobbinsassoc.com/images/icons/md/plato-16.png)](http://mrobbinsassoc.com/projects/keyprism3/plato/files/assets_scripts_vendor_codemirror_codemirror_js/index.html) - plato
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_VqlsSNqGAqAAAhUa.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%33%26webthought_id%3D%31%38%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FanacSNqGAqAAAhVY%2FT%31mcSNqGAqAAAhVn%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FKrg_iNqGAqAAAhMR)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cutil%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%33%26webthought_id%3D%31%38%33%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FanacSNqGAqAAAhVY%2FT%31mcSNqGAqAAAhVn%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FKrg_iNqGAqAAAhMR) `util` - Plato Codemirror Library Script Utils

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Ccodemirror.js) `codemirror.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5Cjavascript.js) `javascript.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CREADME.html) `README.html`

