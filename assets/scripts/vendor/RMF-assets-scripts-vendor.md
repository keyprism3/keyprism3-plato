## RMF-assets-scripts-vendor

`C:\_\keyprism3\._plato\assets\scripts\vendor`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `181`
- `thought_uid` - `9B6F4713-EF7B-F016-2EF4-972FD2368F0D`
- `vp_url` - `keyprism3.vpp://shape/ZlPMSNqGAqAAAhTq/snAsSNqGAqAAAhT3`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/.NlfiNqGAqAAAhLv`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor) _Plato Vender Library Scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%39%26webthought_id%3D%31%37%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FD%39m%30SNqGAqAAAhRx%2F%35bu%30SNqGAqAAAhR.%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fl%39CfiNqGAqAAAhLP) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FZlPMSNqGAqAAAhTq%2FsnAsSNqGAqAAAhT%33) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FZlPMSNqGAqAAAhTq%2FsnAsSNqGAqAAAhT%33) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%39B%36F%34%37%31%33-EF%37B-F%30%31%36-%32EF%34-%39%37%32FD%32%33%36%38F%30D) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/scripts/vendor/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-181) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_ZlPMSNqGAqAAAhTq.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%32%26webthought_id%3D%31%38%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FVqlsSNqGAqAAAhUa%2FrNNsSNqGAqAAAhUp%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fds%37fiNqGAqAAAhMC)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Ccodemirror%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%32%26webthought_id%3D%31%38%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FVqlsSNqGAqAAAhUa%2FrNNsSNqGAqAAAhUp%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fds%37fiNqGAqAAAhMC) `codemirror` - Plato Codemirror Library Scripts

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Cbootstrap-popover.js) `bootstrap-popover.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Cbootstrap-tooltip.js) `bootstrap-tooltip.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Cjquery-%31.%38.%33.min.js) `jquery-1.8.3.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Cjquery.fittext.js) `jquery.fittext.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Clodash.min.js) `lodash.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Cmorris.min.js) `morris.min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5Craphael-min.js) `raphael-min.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5CREADME.html) `README.html`

