## README

`C:\_\keyprism3\._plato\assets\scripts\bundles`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `180`
- `thought_uid` - `8EBDA7F1-89FB-003B-D8FD-E62C32B6DEFB`
- `vp_url` - `keyprism3.vpp://shape/4CcMSNqGAqAAAhS6/ZKiMSNqGAqAAAhTJ`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/dzufiNqGAqAAAhLi`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles) _Plato Codemirror Scripts_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%39%26webthought_id%3D%31%37%39%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FD%39m%30SNqGAqAAAhRx%2F%35bu%30SNqGAqAAAhR.%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2Fl%39CfiNqGAqAAAhLP) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2F%34CcMSNqGAqAAAhS%36%2FZKiMSNqGAqAAAhTJ) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2F%34CcMSNqGAqAAAhS%36%2FZKiMSNqGAqAAAhTJ) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%38EBDA%37F%31-%38%39FB-%30%30%33B-D%38FD-E%36%32C%33%32B%36DEFB) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/scripts/bundles/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-180) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_4CcMSNqGAqAAAhS6.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles%5Ccodemirror.js) `codemirror.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles%5Ccore-bundle.js) `core-bundle.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles%5CREADME.html) `README.html`

