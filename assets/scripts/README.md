## README

`C:\_\keyprism3\._plato\assets\scripts`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `179`
- `thought_uid` - `0DD9445B-12DA-4EFC-EAE7-620872A9B77B`
- `vp_url` - `keyprism3.vpp://shape/D9m0SNqGAqAAAhRx/5bu0SNqGAqAAAhR.`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/l9CfiNqGAqAAAhLP`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts) _Plato Scripts and Script Directories_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%35%26webthought_id%3D%31%37%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FCRF%34SNqGAqAAAhN%30%2F.zt%34SNqGAqAAAhOD%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FZxSviNqGAqAAAhKL) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FD%39m%30SNqGAqAAAhRx%2F%35bu%30SNqGAqAAAhR.) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FD%39m%30SNqGAqAAAhRx%2F%35bu%30SNqGAqAAAhR.) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%30DD%39%34%34%35B-%31%32DA-%34EFC-EAE%37-%36%32%30%38%37%32A%39B%37%37B) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/scripts/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-179) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_D9m0SNqGAqAAAhRx.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%30%26webthought_id%3D%31%38%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34CcMSNqGAqAAAhS%36%2FZKiMSNqGAqAAAhTJ%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FdzufiNqGAqAAAhLi)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cbundles%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%30%26webthought_id%3D%31%38%30%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%34CcMSNqGAqAAAhS%36%2FZKiMSNqGAqAAAhTJ%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FdzufiNqGAqAAAhLi) `bundles` - Plato Codemirror Scripts
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%31%26webthought_id%3D%31%38%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FZlPMSNqGAqAAAhTq%2FsnAsSNqGAqAAAhT%33%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F.NlfiNqGAqAAAhLv)[![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cvendor%5CDesktop.ini%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%31%26webthought_id%3D%31%38%31%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FZlPMSNqGAqAAAhTq%2FsnAsSNqGAqAAAhT%33%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2F.NlfiNqGAqAAAhLv) `vendor` - Plato Vender Library Scripts

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Ccodemirror.markpopovertext.js) `codemirror.markpopovertext.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cplato-display.js) `plato-display.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cplato-file.js) `plato-file.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cplato-overview.js) `plato-overview.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5Cplato-sortable-file-list.js) `plato-sortable-file-list.js`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Cscripts%5CREADME.html) `README.html`

