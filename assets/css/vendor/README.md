## README

`C:\_\keyprism3\._plato\assets\css\vendor`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `177`
- `thought_uid` - `55D080DB-BFDE-8E34-CAEC-5F4936A7E4F3`
- `vp_url` - `keyprism3.vpp://shape/OKbkSNqGAqAAAhQK/WLnkSNqGAqAAAhQZ`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/sDTviNqGAqAAAhKp`
- `FmColorName` - `gray-dark`
- `ColorHex` - `#7D7D85`
- `ColorRgb` - `125,125,133`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor) _Plato Vender Styles_

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%36%26webthought_id%3D%31%37%36%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2F%33QvESNqGAqAAAhPM%2FvG%34kSNqGAqAAAhPb%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FSKZviNqGAqAAAhKa) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FOKbkSNqGAqAAAhQK%2FWLnkSNqGAqAAAhQZ) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2FOKbkSNqGAqAAAhQK%2FWLnkSNqGAqAAAhQZ) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/%35%35D%30%38%30DB-BFDE-%38E%33%34-CAEC-%35F%34%39%33%36A%37E%34F%33) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/src/master/assets/css/vendor/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-177) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_OKbkSNqGAqAAAhQK.html) - visual paradigm publish

### Anticipates

_none_

### Folders

_none_

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor%5Cbootstrap.css) `bootstrap.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor%5Ccodemirror.css) `codemirror.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor%5Cfont-awesome.css) `font-awesome.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_css-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor%5Cmorris.css) `morris.css`
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5Ccss%5Cvendor%5CREADME.html) `README.html`

