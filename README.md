## README

`C:\_\keyprism3\._plato`

- `brain_uid` - `09335629-E824-3836-6E1A-8A30325D2C09`
- `thought_id` - `4`
- `webthought_id` - `4`
- `thought_uid` - `BE73C7F3-1479-43FC-8AC9-408AD2458049`
- `vp_url` - `keyprism3.vpp://shape/k.58SNqGAqAAAhXC/8MN8SNqGAqAAAhXO`
- `vp_url_overview` - `keyprism3.vpp://shape/YcUAPVqGAqAAAhK3/o4jOPVqGAqAAAh0C`
- `FmColorName` - `red-dark`
- `ColorHex` - `#F43F2A`
- `ColorRgb` - `244,63,42`

[![](http://mrobbinsassoc.com/images/icons/md/fm/fm-red-dark-48.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato) _Plato Report Output_

[ ![Codeship Status for keyprism3/keyprism3-plato](https://app.codeship.com/projects/7f4eaa90-ecf8-0134-977a-7ab4e0ed4895/status?branch=master)](https://app.codeship.com/projects/208469)

### Local Links

- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-purple-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%32%26webthought_id%3D%32%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FU.GAPVqGAqAAAhLD) - parent
- [![](http://mrobbinsassoc.com/images/icons/md/_ini-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5CDesktop.ini) - ini
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2Fk.%35%38SNqGAqAAAhXC%2F%38MN%38SNqGAqAAAhXO) - vplink
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](aip://vplink/keyprism%33.vpp%3A%2F%2Fshape%2Fk.%35%38SNqGAqAAAhXC%2F%38MN%38SNqGAqAAAhXO) - vpolink
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](aip://thoughtuid/BE%37%33C%37F%33-%31%34%37%39-%34%33FC-%38AC%39-%34%30%38AD%32%34%35%38%30%34%39) - thought

### External Links

- [![](http://mrobbinsassoc.com/images/icons/md/bitbucket-16.png)](https://bitbucket.org/keyprism3/keyprism3-plato/) - bitbucket
- [![](http://mrobbinsassoc.com/images/icons/md/webbrain-16.png)](https://webbrain.com/brainpage/brain/09335629-E824-3836-6E1A-8A30325D2C09#-4) - webbrain
- [![](http://mrobbinsassoc.com/images/icons/md/visualparadigm-16.png)](http://mrobbinsassoc.com/projects/keyprism3/vp/publish/index.html?url=http://mrobbinsassoc.com/projects/keyprism3/vp/publish/content/PackageDiagram_k.58SNqGAqAAAhXC.html) - visual paradigm publish

### Anticipates

_none_

### Folders

- ![](http://mrobbinsassoc.com/images/icons/md/fm/fm-black-dark-16.png) `.git`
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cassets%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%37%35%26webthought_id%3D%31%37%35%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FCRF%34SNqGAqAAAhN%30%2F.zt%34SNqGAqAAAhOD%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FZxSviNqGAqAAAhKL) `assets` - Plato styles, fonts and scripts
- [![](http://mrobbinsassoc.com/images/icons/md/fm/fm-gray-dark-16.png)](aip://open/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cfiles%5CREADME.md%3Fbrain_uid%3D%30%39%33%33%35%36%32%39-E%38%32%34-%33%38%33%36-%36E%31A-%38A%33%30%33%32%35D%32C%30%39%26thought_id%3D%31%38%34%26webthought_id%3D%31%38%34%26vp_url%3Dkeyprism%33.vpp%3A%2F%2Fshape%2Fn%35o%38SNqGAqAAAhWR%2Fmgk%38SNqGAqAAAhWe%26vp_url_overview%3Dkeyprism%33.vpp%3A%2F%2Fshape%2FYcUAPVqGAqAAAhK%33%2FWlm_iNqGAqAAAhMp) `files` - html index, scripts, and json per file - `both_externals_js/index.html'

### Files

- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cdisplay.html) `display.html` - Summary Display Page
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Cindex.html) `index.html` - Main Page
- [![](http://mrobbinsassoc.com/images/icons/md/_html-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5CREADME.html) `README.html`
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Creport.history.js) `report.history.js` - Historical Data
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Creport.history.json) `report.history.json` - Historical Data
- [![](http://mrobbinsassoc.com/images/icons/md/_js-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Creport.js) `report.js` - Current Data
- [![](http://mrobbinsassoc.com/images/icons/md/_json-16.png)](aip://slickedit/C%3A%5C_%5Ckeyprism%33%5C._plato%5Creport.json) `report.json` - Current Data

